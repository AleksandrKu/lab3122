<nav class="navbar navbar-inverse">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Project name</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li <?php if(isset($_GET['page'])) { if ($_GET['page'] == "home" || empty($_GET['page'])) { ?> class="active"<? }} else { ?> class="active"<? } ?>><a href="/?page=home">Home</a></li>
				<li <?php if(isset($_GET['page'])) { if ($_GET['page'] == "about") { ?> class="active" <? } }?>><a href="/?page=about">About</a></li>
				<li <?php if(isset($_GET['page'])) { if ($_GET['page'] == "contacts") { ?> class="active" <? } }?>><a href="/?page=contacts">Contact</a></li>
				<li <?php if(isset($_GET['page'])) { if ($_GET['page'] == "currency") { ?> class="active" <? } }?>><a href="/?page=currency">Currency</a></li>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</nav>