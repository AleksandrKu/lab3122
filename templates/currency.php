<?php
$currencies = [
	"USD",
	"RUB",
	"JPY",
	"EUR",
	"BRL",
	"CAD",
	"CHF",
	"CNY",
	"CZK",
	"DKK",
	"GBP",
	"HKD",
	"HRK",
	"HUF",
	"IDR",
	"ILS",
	"INR",
	"KRW",
];
$api_url = "http://api.fixer.io/latest"; // url сайта к которому будем отправлять запрос
if (!empty($_POST['base_currency']) and !empty($_POST['currency_list'])) {  // проверка чтобы пользователем был выполнен выбор
	if (in_array($_POST['base_currency'], $currencies)) {  // проверка есть ли в массиве пост значение base_currency, т.е. нажата ли кнопка отправить
		$params_array = array(
			'base' => $_POST['base_currency'],
			'symbols' => implode(",", $_POST['currency_list']) // массив преобразуем в строку, значения через запятую
		); // преобразуемм массив POST  в нужную нам форму для отправки запроса
		$request_url = $api_url . "?" . http_build_query($params_array); //формируем URI запроса методом GET
		$result = file_get_contents($request_url);  // получаем информацию от сайта fixer.io по GET запросу в виде строки
       	$result_array = json_decode($result, true); //   преобразует полученную информацию в массив
      	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Currency</title>
    <!-- Bootstrap -->
    <link href="<?= PUBLIC_DIR ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="my.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .floatt {
            background-color: #eafdff;
            float: left;
            margin: 10px;
            text-align: left;
            text-decoration-color: #0f0f0f;
        }
    </style>
</head>
<body>
<?php
require_once "template_head.php";  // подключение меню
?>
<div class="container">
    <div class="starter-template">
        <h1>Currency.</h1>
        <p class="lead">Currency.<br></p>
    </div>
    <div class="floatt">
        <form method="post">
            <span>Base currency:</span>
            <select class="" name="base_currency">
				<?php
				foreach ($currencies as $currency) {  // проходим по массиву со всеми валютами
					$selectedForOption = "";
					if ($currency == $params_array['base']) {  // узнаем какая валюта была выбрана, для отображения ее после отправки формы
						$selectedForOption = "selected";  // после отправки запроса оставит выбранную валюту
					}
					echo "<option {$selectedForOption} value='{$currency}' > {$currency}</option>"; // формирование выпадающего списка валют
				}
				?>
            </select><br><br>
            <h4> Currencies list</h4>
			<?php
			foreach ($currencies as $currency) {
				$checked = '';
				if (!empty($result_array)) {
					if (in_array($currency, $_POST['currency_list'])) {  // выставляет выбранные валюты в чекбоксах
						$checked = "checked";
					}
				}
				echo "<input type='checkbox' name='currency_list[]' value='{$currency}' {$checked} >{$currency}<br>";

			} ?>
            <input type="submit" value="Отправить">
        </form>
    </div>
    <div class="floatt"></div>
	<?php if (!empty($result_array)) { ?>
        <!--Если был запрос на сервер отображаем полученный результат-->
        <div class="floatt"><h4>Currency rates  <?= $result_array['date'] ?> </h4>
			<?php
			foreach ($result_array['rates'] as $current => $value) {
				echo "<p> 1 " . $result_array['base'] . " = " . $value . " " . $current;
			}
			?>
        </div>
	<?php } ?>
</div><!-- /.container -->
<?php
require_once "template_footer.php";  //bootstrap and jquery
?>
</body>
</html>
